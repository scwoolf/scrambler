class HomeController < ApplicationController
  before_action :logged_out, only: [:welcome]
  
  def welcome
  end
  
  def info
    unless cookies[:return_to_url].nil?
      @return_to_url = cookies[:return_to_url]
    else
      @return_to_url = home_url
    end
  end
  
end
