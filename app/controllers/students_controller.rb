class StudentsController < ApplicationController
  
  def new
    @course = Course.find_by(id: params[:course_id])
  end
    
  def create
    @course = Course.find_by(id: params[:course_id])
    if params[:student][:name].blank?
      flash.now[:error] = ["Name can't be blank"]
      render 'new'
    else
    params[:student][:name].split("\n").each do |name|
    @student = Student.create({name: name.strip, section_id: params[:section_id]})
     if @student.save and @student.course.standards.any?
      @student.course.standards.each do |standard|
        StudentSkill.create({student_id: @student.id, standard_id: standard.id, skill: 3 })
      end
     end
    end
    redirect_to user_course_sections_path(user_id: params[:user_id], course_id: params[:course_id])
    end
  end
  
  def destroy
    Student.find_by(id: params[:id]).destroy
    redirect_to user_course_sections_path(user_id: params[:user_id], course_id: params[:course_id])
  end
  
  def show
    @student = Student.find_by(id: params[:id])
  end
  
  def edit
    @student = Student.find_by(id: params[:id])
  end
  
  def update
    @student = Student.find_by(id: params[:id])
    if @student.update(name: params[:student][:name])
      redirect_to user_course_sections_path(user_id: current_user.id, course_id: @student.course.id)
    else
      flash.now[:error] = @student.errors.full_messages
      render 'edit'
    end
  end
  
end
