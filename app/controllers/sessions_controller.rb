class SessionsController < ApplicationController
  before_action :logged_out, only: [:new, :create]
  
  def new
  end
  
  def create
    user = User.find_by(email: params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      login user
      redirect_to user_courses_path(user_id: current_user.id)
    else
      render 'new'
    end
  end
  
  def destroy
    logout if logged_in?
    redirect_to home_path
  end
  
end
