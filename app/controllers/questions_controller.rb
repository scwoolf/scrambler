class QuestionsController < ApplicationController
  before_action :correct_user
  
  def new
    @course = Course.find_by(id: params[:course_id])
  end
  
  def create
    @course = Course.find_by(id: params[:course_id])
    @question = Question.new({name: params[:question][:name], body: params[:question][:body], answer: params[:question][:answer], exam_id: params[:exam_id]})
    if @question.save
      QuestionSkill.create({standard_id: params[:question][:standard], question_id: @question.id, skill: params[:question][:skill]})
      redirect_to user_course_exams_path(user_id: params[:user_id], course_id: params[:course_id])
    else
      flash[:error] = @question.errors.full_messages
      redirect_to new_user_course_exam_question_path(user_id: params[:user_id], course_id: params[:course_id], exam_id: params[:exam_id])
    end
  end
  
  def destroy
    Question.find_by(id: params[:id]).destroy
    redirect_to user_course_exams_path(user_id: params[:user_id], course_id: params[:course_id])
  end
  
  def edit
    @question = Question.find_by(id: params[:id])
  end
  
  def update
    @question = Question.find_by(id: params[:id])
    @question_skill = @question.question_skill
    if @question.update(name: params[:question][:name], body: params[:question][:body], answer: params[:question][:answer], exam_id: params[:exam_id])
      @question_skill.update(standard_id: params[:question][:standard], question_id: @question.id, skill: params[:question][:skill])
      redirect_to user_course_exams_path(user_id: params[:user_id], course_id: params[:course_id])
    else
      flash.now[:error] = @question.errors.full_messages
      render 'edit'
    end
  end
  
end
