class CoursesController < ApplicationController
  before_action :correct_user
  
  def new
  end
  
  def create
    if params[:course][:name].blank?
      flash.now[:error] = ["Name can't be blank"]
      render 'new'
    else
      params[:course][:name].split("\n").each do |name|
      @course = Course.create({name: name.strip, user_id: params[:user_id]})
      end
    redirect_to user_courses_path(user_id: params[:user_id])
    end
  end
  
  def destroy
    Course.find_by(id: params[:id]).destroy
    redirect_to user_courses_path(user_id: current_user.id)
  end
  
  def index
    @courses = current_user.courses
  end
  
  def edit
    @course = Course.find_by(id: params[:id])
  end
  
  def update
    @course = Course.find_by(id: params[:id])
    if @course.update(name: params[:course][:name])
      redirect_to user_courses_path(user_id: current_user.id)
    else
      flash.now[:error] = @course.errors.full_messages
      render 'edit'
    end
  end
    
end
