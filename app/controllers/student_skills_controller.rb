class StudentSkillsController < ApplicationController
  before_action :correct_user
  
  def increment
    @student_skill = StudentSkill.find_by(id: params[:id])
    @student_skill.update(skill: @student_skill.skill + 1)
    redirect_to user_course_section_student_path(user_id: params[:user_id], course_id: params[:course_id], section_id: params[:section_id], id: @student_skill.student.id)
  end
  
  def decrement
    @student_skill = StudentSkill.find_by(id: params[:id])
    @student_skill.update(skill: @student_skill.skill - 1)
    redirect_to user_course_section_student_path(user_id: params[:user_id], course_id: params[:course_id], section_id: params[:section_id], id: @student_skill.student.id)
  end
  
end
