class SectionsController < ApplicationController
  before_action :correct_user
  
  def new
    @course = Course.find_by(id: params[:course_id])
  end
  
  def create
    @course = Course.find_by(id: params[:course_id])
    if params[:section][:name].blank?
      flash.now[:error] = ["Name can't be blank"]
      render 'new'
    else
    params[:section][:name].split("\n").each do |name|
      @section = Section.create({name: name.strip, course_id: params[:course_id]})
    end
    redirect_to user_course_sections_path(user_id: params[:user_id], course_id: params[:course_id])
    end
  end
  
  
  def destroy
    Section.find_by(id: params[:id]).destroy
    redirect_to user_course_sections_path(user_id: params[:user_id], course_id: params[:course_id])
  end
  
  def index
  @course = Course.find_by(id: params[:course_id])
  end
  
  def edit
    @section = Section.find_by(id: params[:id])
  end
  
  def update
    @section = Section.find_by(id: params[:id])
    if @section.update(name: params[:section][:name])
      redirect_to user_course_sections_path(user_id: current_user.id, course_id: @section.course.id)
    else
      flash.now[:error] = @section.errors.full_messages
      render 'edit'
    end
  end
  
    
end
