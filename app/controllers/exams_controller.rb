class ExamsController < ApplicationController
  before_action :correct_user
  
  def index
    @course = Course.find_by(id: params[:course_id])
  end
  
  def new
    @course = Course.find_by(id: params[:course_id])
  end
    
  def create
    @course = Course.find_by(id: params[:course_id])
    if params[:exam][:name].blank?
      flash.now[:error] = ["Name can't be blank"]
      render 'new'
    else
    params[:exam][:name].split("\n").each do |name|
    @exam = Exam.create({name: name.strip, course_id: params[:course_id]})
    end
   redirect_to user_course_exams_path(user_id: @exam.author.id, course_id: @exam.course.id)
    end
  end
    
    def destroy
      Exam.find_by(id: params[:id]).destroy
      redirect_to user_course_exams_path(user_id: params[:user_id], course_id: params[:course_id])
    end
    
    def edit
      @exam = Exam.find_by(id: params[:exam_id])
    end
    
    def update
    @exam = Exam.find_by(id: params[:id])
    if @exam.update(name: params[:exam][:name])
      redirect_to user_course_exams_path(user_id: current_user.id, course_id: @exam.course.id)
    else
      flash.now[:error] = @exam.errors.full_messages
      render 'edit'
    end
    end
    
    def publish
      @exam = Exam.find_by(id: params[:exam_id])
    end
    
    def scramble
      @exam = Exam.find_by(id: params[:exam_id])
      if @exam.questions.count < params[:publish][:number_of_questions].to_i
        flash[:alert] = "Desired number of questions cannot exceed available questions!"
        redirect_to publish_user_course_exam_path(user_id: current_user.id, course: @exam.course.id, id: @exam.id)
      else
      exam_name = @exam.name
      course_name = @exam.course.name
      school_name = @exam.author.school
      student_exams = @exam.scramble(params[:publish][:who],params[:publish][:number_of_questions])
      respond_to do |format|
        
        format.pdf do
          pdf = Prawn::Document.new
          student_exams.each do |student_exam|
        
          student = Student.find_by(id: student_exam[0])
          student_name = student.name
          section_name = student.section.name
        
          pdf.text student_name + ', ' + section_name
          pdf.text course_name + ', ' + school_name
          pdf.move_down 20
          pdf.text exam_name, :align => :center 
          pdf.move_down 20
        
          queries = student_exam[1]
          queries.each_with_index do |query, idx|
            n = idx + 1
            query_text = n.to_s + '. ' + Question.find_by(id: query).body
            pdf.text query_text
            pdf.move_down 100
          end
          
          pdf.start_new_page
          
          end
      
          if params[:publish][:answer_key].to_i == 1
            student_exams.each do |student_exam|
          
            student = Student.find_by(id: student_exam[0])
            student_name = student.name
            section_name = student.section.name
          
            pdf.text student_name + ', ' + section_name
            pdf.text course_name + ', ' + school_name
            pdf.move_down 20
            pdf.text exam_name + " Answer Key", :align => :center
            pdf.move_down 20
        
            queries = student_exam[1]
            queries.each_with_index do |query, idx|
              n = idx + 1
              answer_text = n.to_s + '. ' + Question.find_by(id: query).answer
              pdf.text answer_text
              pdf.move_down 10
            end
            pdf.start_new_page
            end
          end
        send_data pdf.render,
        filename: "#{exam_name}.pdf",
        type: 'application/pdf',
        disposition: 'inline'
      end
    end
      end
    end
end

      