class UsersController < ApplicationController
  before_action :logged_out, only: [:new, :create]
  before_action :correct_user, only: [:show]
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      login(@user)
      redirect_to user_courses_path(user_id: current_user.id)
    else
      flash.now[:error] = @user.errors.full_messages
      render 'new'
    end
  end
  
  def show
    @user = User.find_by(id: params[:id])
    @courses = @user.courses
  end
  
  private
  
  def user_params
    params.require(:user).permit(:name, :school, :email, :password)
  end
  
end
