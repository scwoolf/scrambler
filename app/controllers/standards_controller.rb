class StandardsController < ApplicationController
  before_action :correct_user
    
  def new
  end
  
   def create
     if params[:standard][:name].blank?
       flash.now[:error] = ["Name can't be blank"]
       render 'new'
     else
    params[:standard][:name].split("\n").each do |name|
    @standard = Standard.create({name: name.strip, course_id: params[:course_id]})
    if @standard.save and @standard.course.students.any?
      @standard.course.students.each do |student|
        StudentSkill.create({student_id: student.id, standard_id: @standard.id, skill: 3 })
      end
    end
    end
     redirect_to user_courses_path(user_id: current_user.id)
     end
   end
  
  def destroy
    Standard.find_by(id: params[:id]).destroy
    redirect_to user_courses_path(user_id: current_user.id)
  end
    
  def edit
    @standard = Standard.find_by(id: params[:id])
  end
  
  def update
    @standard = Standard.find_by(id: params[:id])
    if @standard.update(name: params[:standard][:name])
      redirect_to user_courses_path(user_id: current_user.id)
    else
      flash.now[:error] = @standard.errors.full_messages
      render 'edit'
    end
  end
  
end
