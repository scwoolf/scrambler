class Question < ApplicationRecord
  validates :name, presence: true
  validates :body, presence: true
  validates :answer, presence: true
  validates :exam_id, presence: true
  belongs_to :exam
  has_one :question_skill, foreign_key: :question_id, dependent: :destroy
  
  def author
    exam.author
  end
  
  def course
    exam.course
  end
  
  def standard
    question_skill.standard
  end
  
  def skill
    question_skill.skill
  end
  
end
