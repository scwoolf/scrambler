class User < ApplicationRecord
  attr_accessor :persistence_token
  validates :name, presence: true
  validates :email, presence: true, uniqueness: true,
  format: {with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i}
  validates :password, presence: true
  has_secure_password
  has_many :courses, foreign_key: :user_id, dependent: :destroy
  
  def persist
    self.persistence_token = SecureRandom.urlsafe_base64
    update_attribute(:persistence_digest, BCrypt::Password.create(persistence_token))
  end
  
  def cease
    update_attribute(:persistence_digest, nil)
  end
  
  def authenticate?(persistence_token)
    BCrypt::Password.new(persistence_digest).is_password?(persistence_token)
  end
  
end
