class StudentSkill < ApplicationRecord
  validates :student_id, presence: true
  validates :standard_id, presence: true
  validates :skill, presence: true, numericality: {only_integer: true, less_than_or_equal_to: 5, greater_than_or_equal_to: 1} 
  belongs_to :student
  belongs_to :standard
  
  def display
    result = ''
    skill.times {result << '★'}
    result
  end
  
end
