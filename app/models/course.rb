class Course < ApplicationRecord
  validates :name, presence: true
  validates :user_id, presence: true
  belongs_to :user
  has_many :standards, foreign_key: :course_id, dependent: :destroy
  has_many :exams, foreign_key: :course_id, dependent: :destroy
  has_many :sections, foreign_key: :course_id, dependent: :destroy
  has_many :students, through: :sections
  
  def standards_hash
    sh = []
    standards.each do |standard|
      sh << [standard.name, standard.id]
    end
    sh
  end
  
end
