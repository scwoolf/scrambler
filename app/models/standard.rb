class Standard < ApplicationRecord
  belongs_to :course
  validates :name, presence: true
  validates :course_id, presence: true
  has_many :student_skills, foreign_key: :standard_id, dependent: :destroy
  has_many :question_skills, foreign_key: :standard_id, dependent: :destroy
end
