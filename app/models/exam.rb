class Exam < ApplicationRecord
  belongs_to :course
  has_many :questions, foreign_key: :exam_id, dependent: :destroy
  validates :name, presence: true
  validates :course_id, presence: true
  
  def author
    course.user
  end
  
  def recipients_hash
    rh = [['All students',1]]
    course.sections.each do |section|
      rh << [ section.name, section.id * 10 + 2 ]
    end
    course.sections.each do |section|
      section.students.each do |student|
        rh << [student.name, student.id * 10 + 3]
      end
    end
    rh
  end
  
  def scramble(idx, number_of_questions)
    students = recipients(idx)
    student_exams = []
    students.each do |student|
      student_exam = []
      student_exam << student.id
      pm = probability_matrix(student)
      uv = unique_select(pm, number_of_questions.to_i)
      student_exam << uv
      student_exams << student_exam
    end
    student_exams
  end
  
  def recipients(n)
    recipients = []
    case n[-1].to_i
    when 3
      recipients << Student.find_by(id: n[0..-2].to_i) 
    when 2
      Section.find_by(id: n[0..-2].to_i).students.each do |student|
        recipients << student
      end
    when 1
      course.sections.each do |section|
        section.students.each do |student|
          recipients << student
        end
      end
    end
    recipients
  end
  
  def probability(student_score, question_score)
    (6 - student_score)*(5 - (student_score - question_score).abs)
  end
  
  def probability_matrix(student)
    matrix = []
    questions.each do |question|
      question_score = question.skill
      student_score = StudentSkill.where(student_id: student.id, standard_id: question.standard.id).first.skill
      n = probability(student_score,question_score)
      n.times do
        matrix << question.id
      end
    end
    matrix
  end
  
  def unique_select(matrix, n)
    unique_values = []
    n.times do
      idx = rand(matrix.length)
      unique_values << matrix[idx]
      matrix.delete(matrix[idx])
    end
    unique_values = unique_values.shuffle
    unique_values
  end
      
end
