class Student < ApplicationRecord
  validates :name, presence: true
  validates :section_id, presence: true
  belongs_to :section
  has_many :student_skills, foreign_key: :student_id, dependent: :destroy
  
  def course
    section.course
  end
  
  def teacher
    course.user
  end
  
end
