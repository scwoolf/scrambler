class QuestionSkill < ApplicationRecord
  validates :question_id, presence: true
  validates :standard_id, presence: true
  validates :skill, presence: true
  belongs_to :question
  belongs_to :standard
  
   def display
    result = ''
    skill.times {result << '★'}
    result
  end
  
end
