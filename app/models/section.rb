class Section < ApplicationRecord
  belongs_to :course
  has_many :students, foreign_key: :section_id, dependent: :destroy
  validates :name, presence: true
  validates :course_id, presence: true
end
