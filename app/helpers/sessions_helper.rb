module SessionsHelper
  
  def login(user)
    user.persist
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:persistence_token] = user.persistence_token
  end
  
  def logout
    current_user.cease
    cookies.delete(:user_id)
    cookies.delete(:persistence_token)
    current_user = nil
  end
  
  def logged_in?
    !current_user.nil?
  end
  
  def current_user
      current_user ||= User.find_by(id: cookies.signed[:user_id])
  end
  
  def current_user?
    user = User.find_by(id: params[:user_id])
    user = User.find_by(id: params[:id]) if user.nil?
    user == current_user
  end
  
  def logged_out
    redirect_to user_courses_path(user_id: current_user.id) if logged_in?
  end
  
  def correct_user
    if logged_in?
      if current_user?
        cookies[:return_to_url] = request.original_url if request.get?
      else
      redirect_to user_courses_path(user_id: current_user.id) 
      end
    else
      redirect_to login_path
    end
  end
  
end
