Rails.application.routes.draw do
  root 'home#welcome', as: 'home'
  get '/info', to: 'home#info'
  resources :users do
    resources :courses do
      resources :standards
      resources :exams do
        resources :questions
      end
      resources :sections do
        resources :students
      end
    end
  end
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  get '/user/:user_id/courses/:course_id/exams/:exam_id/publish', to: 'exams#publish', as: 'publish_user_course_exam'
  post '/user/:user_id/courses/:course_id/exams/:exam_id/scramble', to: 'exams#scramble', as: 'scramble_user_course_exam'
  put '/users/:user_id/courses/:course_id/sections/:section_id/students/:student_id/student_skill/:id', to: 'student_skills#increment', as: 'increment'
  patch '/users/:user_id/courses/:course_id/sections/:section_id/students/:student_id/student_skill/:id', to: 'student_skills#decrement', as: 'decrement'
  
end
