class RemoveNumberOfQuestionsFromExams < ActiveRecord::Migration[6.0]
  def change

    remove_column :exams, :number_of_questions, :integer
  end
end
