class RemoveDescriptionFromStandards < ActiveRecord::Migration[6.0]
  def change

    remove_column :standards, :description, :text
  end
end
