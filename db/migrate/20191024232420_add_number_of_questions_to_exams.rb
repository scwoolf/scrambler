class AddNumberOfQuestionsToExams < ActiveRecord::Migration[6.0]
  def change
    add_column :exams, :number_of_questions, :integer
  end
end
