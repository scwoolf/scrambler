class AddIndexToQuestionSkills < ActiveRecord::Migration[6.0]
  def change
    
    add_index :question_skills, [:question_id, :standard_id], unique: true
    
  end
end
