class CreateStudentSkills < ActiveRecord::Migration[6.0]
  def change
    create_table :student_skills do |t|
      t.integer :skill
      t.references :student, null: false, foreign_key: true
      t.references :standard, null: false, foreign_key: true
      
      t.timestamps
    end
  end
end
