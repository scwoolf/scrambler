class AddNameToStandards < ActiveRecord::Migration[6.0]
  def change
    add_column :standards, :name, :string
  end
end
