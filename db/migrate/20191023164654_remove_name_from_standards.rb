class RemoveNameFromStandards < ActiveRecord::Migration[6.0]
  def change

    remove_column :standards, :name, :string
  end
end
