class AddIndexToStudentSkills < ActiveRecord::Migration[6.0]
  def change
    
    add_index :student_skills, [:student_id, :standard_id], unique: true
    
  end
end
