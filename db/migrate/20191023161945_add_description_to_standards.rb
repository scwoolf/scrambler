class AddDescriptionToStandards < ActiveRecord::Migration[6.0]
  def change
    add_column :standards, :description, :text
  end
end
