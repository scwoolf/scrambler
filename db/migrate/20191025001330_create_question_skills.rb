class CreateQuestionSkills < ActiveRecord::Migration[6.0]
  def change
    create_table :question_skills do |t|
      t.references :standard, null: false, foreign_key: true
      t.references :question, null: false, foreign_key: true
      t.integer :skill
      t.timestamps
    end
  end
end
