class AddPersistenceDigestToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :persistence_digest, :string
  end
end
